/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isp.shop.ui;

import java.lang.reflect.*;
import javax.swing.table.AbstractTableModel;

import isp.shop.model.Shop;
import isp.shop.model.Product;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yenner
 */
public class ProductListModel extends AbstractTableModel{
    
    private static final String[] NAMES = {"code", "name", "price"};
    private static final String[] TITLES = {"code", "name", "price"};
    
    private final Shop shop;
    
    public ProductListModel(Shop shop){
        this.shop = shop;
    }
    
    @Override
    public String getValueAt(int row, int column){
        String key = NAMES[column];
        Product product = (Product)shop.getCatalogue()[row];
        Object value;
        if(null == key){
            value = product.getPrice();
        }
        else switch (key) {
            case "code":
                value = product.getCode();
                break;
            case "name":
                value = product.getName();
                break;
            default:
                value = product.getPrice();
                break;
        }
        
        return value.toString();
    }
    
    @Override
    public int getRowCount(){
        int count = shop.getInventory();
        return count;
    }
    
    @Override
    public String getColumnName(int col)
    {
        return TITLES[col];
    }
    
    @Override
    public int getColumnCount(){
        return TITLES.length;
    }
}
