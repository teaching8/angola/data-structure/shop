/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isp.shop.model;

/**
 *
 * @author yenner
 */
public class Shop {

    public static final int CAPACITY = 3;
    private final Product[] catalogue;
    private int inventory;
    
    public Shop(){
        catalogue = new Product[CAPACITY];
        inventory = 0;
    }
    
    /**
     * Add a product to shopt catalog
     * @param product a product instance
     */
    public void addProduct(Product product){
        // CHANGE HERE
        catalogue[inventory] = product;
        inventory += 1;
    }
    
    /**
     * @return total of products in catalogue
     */
    public int getInventory(){
        return inventory;
    }
    
    /**
     *
     * @return Shop catalog
     */
    public Product[] getCatalogue(){
        return catalogue;
    }
    
}
