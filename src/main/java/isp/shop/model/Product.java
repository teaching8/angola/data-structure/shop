/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isp.shop.model;

/**
 *
 * @author yenner
 */
public class Product{
    
    private String code;
    private String name;
    private float price;
    
    public Product(String code, String name, float price){
        this.code = code;
        this.name = name;
        this.price = price;
    }
    
    public String getName(){
        return code;
    }
    
    public String getCode(){
        return name;
    }
    
    public float getPrice(){
        return price;
    }
    
}
